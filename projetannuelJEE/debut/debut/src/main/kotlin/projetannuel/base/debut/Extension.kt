package projetannuel.base.debut

import java.time.LocalDateTime
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField
import java.util.*

fun LocalDateTime.format() = this.format(frenchDateFormatter)

private val daysLookup = (1..31).associate { it.toLong() to getOrdinal(it) }

private val frenchDateFormatter = DateTimeFormatterBuilder().appendPattern("dd-MM-yyyy").appendLiteral(" ").appendText(ChronoField.DAY_OF_MONTH, daysLookup).appendLiteral(" ").toFormatter(Locale.FRANCE)

private fun getOrdinal(n : Int) = when{
    n in 11..12 -> "${n}ème"
    n in 21..22 -> "${n}ème"
    n in 31..32 -> "${n}ème"
    n % 10 == 1 -> "${n}er"
    else -> "${n}ème"
}

fun String.toSlug() = toLowerCase().replace("\n"," ").replace("[^a-z\\d\\s]".toRegex(), " ").split(" ").joinToString("-").replace("-+".toRegex(), "-")