package projetannuel.base.debut

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DebutConfiguration{

    @Bean
    fun databaseInitializer(userRepository: UserRepository, articleRepository: ArticleRepository) = ApplicationRunner{
        val smaldini = userRepository.save(User("Jakc","Jack'O","Lantern"))
        articleRepository.save(Article(title="Reactor Bismuth is out", headline="Lorem ipsum", content="dolor sit amet", author= smaldini))
        articleRepository.save(Article(title="Reactor aluminium has landed", headline="Lorem ipsum", content="dolor si amet", author= smaldini))
    }
}