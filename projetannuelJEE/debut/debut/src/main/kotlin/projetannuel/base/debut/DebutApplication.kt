package projetannuel.base.debut

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(DebutProperties::class)
class DebutApplication

fun main(args: Array<String>) {
	runApplication<DebutApplication>(*args){
		setBannerMode(Banner.Mode.OFF)
	}
}
