package projetannuel.base.debut

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class RepositoriesTests @Autowired constructor(
        val entityManager: TestEntityManager,
        val userRepository: UserRepository,
        val articleRepository: ArticleRepository
){
    @Test
    fun `When findByIdOrNull then return Article`(){
        val jack = User("JackO", "Jack", "TheReaper")
        entityManager.persist(jack)
        val article = Article("Test Article", "Test de repo", "On fait les tests sur les repos", jack)
        entityManager.persist(article)
        entityManager.flush()
        val found = articleRepository.findByIdOrNull(article.id!!)
        assertThat(found).isEqualTo(article)
    }
     @Test
     fun `When findByLogin then return User`(){
         val jack = User("JackO", "Jack", "TheReaper")
         entityManager.persist(jack)
         entityManager.flush()
         val user = userRepository.findByLogin(jack.login)
         assertThat(user).isEqualTo(jack)
     }
}