DROP TABLE IF EXISTS `demande` cascade;
CREATE TABLE IF NOT EXISTS `demande` (
  `id` bigint(20) NOT NULL auto_increment,
  `accountname` varchar(255) DEFAULT NULL,
  `recherche` varchar(255),
  `info` varchar(255) DEFAULT NULL,
  `favori` varchar(255) default 'non',
  PRIMARY KEY (`id`)
);

