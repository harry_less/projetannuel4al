package service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Resource;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.json.JsonReadFeature;

import Entities.Demande;
import Entities.JsonRecep;
import mappers.DemandeMapper;
import model.DemandeDTO;
import model.NewDemandeDTO;
import repository.DemandeRepository;

@Service
public class DemandeService {

	final AtomicInteger counter = new AtomicInteger();
	final List<Demande> posts = new CopyOnWriteArrayList<Demande>();
	
	@Resource private DemandeRepository demandeRepository;
	
	@Resource private DemandeMapper mapper;
	
	public List<DemandeDTO> getAllDemande(){
		List<Demande> demandeList = demandeRepository.findAll();
		return mapper.mapTo(demandeList);
	}
	
	public NewDemandeDTO addDemande(NewDemandeDTO d) {
		Demande de = mapper.mapToNewDemande(d);
		String uri = "https://localhost:8080/?username="+d.profil;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> request = new HttpEntity<String>(d.profil, headers);
		JsonRecep result2 = restTemplate.getForObject(uri, JsonRecep.class);
		de.setInfo(result2);
		demandeRepository.save(de);
		return mapper.mapToNewDemande(de);
	}
	
	public void deleteDemande(DemandeDTO dt) {
		demandeRepository.delete(mapper.mapTo(dt));
	}
	
	public DemandeDTO getOne(long id) {
		Demande de = demandeRepository.getOne(id);
		return mapper.mapTo(de);
	}
}
