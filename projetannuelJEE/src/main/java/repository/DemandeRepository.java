package repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Entities.Demande;

@Repository
public interface DemandeRepository extends JpaRepository<Demande, Long> {

	public List<Demande> getAllDemandeByUserId(long id);
}
