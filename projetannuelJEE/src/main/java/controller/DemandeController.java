package controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import model.DemandeDTO;
import model.NewDemandeDTO;
import service.DemandeService;

@Controller
@RequestMapping(path="/demande")
public class DemandeController {

	@Resource DemandeService demandeService;
	
	@RequestMapping(method = RequestMethod.POST, value="/demande/")
	public NewDemandeDTO demande(@RequestBody NewDemandeDTO demande) {
		return demandeService.addDemande(demande);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/historique")
	public List<DemandeDTO> historique(){
		return demandeService.getAllDemande();
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/demande/{id}")
	public DemandeDTO getOne(@PathVariable long id) {
		return demandeService.getOne(id);		
	}
	
	
	 //@RequestMapping(method = RequestMethod.GET, value="/favori")
	
	/*public List<DemandeDTO> getFavori(){
		return demandeService.getFavori();
	}
	*/
	
	// @RequestMapping(method = RequestMethod.GET, value="/favori/{id}")
	
	//public DemandeDTO addFavori(@PathVariable long id) {
		//return demandeService.addFavori(id);
	//}
}
