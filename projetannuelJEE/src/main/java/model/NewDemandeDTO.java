package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NewDemandeDTO {

	public long id;
	public String profil;
}
