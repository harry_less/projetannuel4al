package model;

import Entities.JsonRecep;


public class DemandeDTO {

	private Long id;
	private String accountname;
	private String recherche;
	private JsonRecep info;
	private boolean favori;
	
	public long getId() {
		return this.id;
	}
	
	public void setInfo(JsonRecep info) {
		this.info = info;
	}
	
	public String getAccountname() {
		return this.accountname;
	}
	
	public void setFavori(boolean f) {
		this.favori = f;
	}
	
	public void setRecherche(String r) {
		this.recherche = r;
	}
	
	public JsonRecep getInfo() {
		return this.info;
	}
	
	public boolean getFavori() {
		return this.favori;
	}
	
	public String getRecherche() {
		return this.recherche;
	}
}
