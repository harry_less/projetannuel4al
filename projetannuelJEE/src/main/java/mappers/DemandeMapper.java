package mappers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import Entities.Demande;
import model.DemandeDTO;
import model.NewDemandeDTO;

@Component
public class DemandeMapper {

	
	public Demande mapToNewDemande(NewDemandeDTO ndt) {
		Assert.notNull(ndt, "The demande must be not null");
		Demande d = new Demande();
		BeanUtils.copyProperties(ndt, d);
		return d;
	}
	
	public NewDemandeDTO mapToNewDemande(Demande d) {
		Assert.notNull(d, "The demande must not be null");
		NewDemandeDTO rdt = new NewDemandeDTO();
		BeanUtils.copyProperties(d, rdt);
		return rdt;
	}
	
	public Demande mapTo(DemandeDTO ddto) {
		Assert.notNull(ddto, "the demande must not be null");
		Demande d = new Demande();
		BeanUtils.copyProperties(ddto, d);
		return d;
	}
	
	public DemandeDTO mapTo(Demande d) {
		Assert.notNull(d, "The demande must not be null");
		DemandeDTO rdt = new DemandeDTO();
		BeanUtils.copyProperties(d, rdt);
		return rdt;
	}
	
	public List<DemandeDTO> mapTo(List<Demande> demande){
		Assert.notNull(demande, "the demandelist must not be null");
		return demande.stream().map(Demande -> this.mapTo(Demande)).collect(Collectors.toList());
	}
}
