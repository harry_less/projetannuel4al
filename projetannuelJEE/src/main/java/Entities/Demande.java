package Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="demande")
public class Demande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String accountname;
	private JsonRecep info;
	private boolean favori;
	private String recherche;
	
	public long getId() {
		return this.id;
	}
	
	public void setInfo(JsonRecep info) {
		this.info = info;
	}
	
	public void setAccountname(String p) {
		this.accountname = p;
	}
	
	public void setFavori(boolean f) {
		this.favori = f;
	}
	
	public void setRecherche(String r) {
		this.recherche = r;
	}
	
	public String getAccountname() {
		return this.accountname;
	}
	
	public JsonRecep getInfo() {
		return this.info;
	}
	
	public boolean getFavori() {
		return this.favori;
	}
	
	public String getRecherche() {
		return this.recherche;
	}
}
