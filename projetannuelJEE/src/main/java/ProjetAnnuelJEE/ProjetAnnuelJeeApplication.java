package ProjetAnnuelJEE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetAnnuelJeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetAnnuelJeeApplication.class, args);
	}

}
